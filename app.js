// Importation de dataLayer
var dataTaskLayer = require('./dataLayer/dataTaskLayer');
var dataUserLayer = require('./dataLayer/dataUserLayer');


// Importation des modules importants.
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
var jwtUtils = require('./utils/jwt.utils.js');
var cookie = require('cookie');

// Sel pour bcrypt
const saltRounds = 10;

// PORT dynamique pour heroku ou localhost
const PORT = process.env.PORT || 5000;

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

// Pour l'application mobile
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-with, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
  });

app.get("/", function(req,res){
    res.sendFile(path.join(__dirname + '/index.html'));
});

// Renvoie les listes et leurs tâches
app.get("/getTaskSet", function(req, res){
    var username = undefined;
    // Vérifie si l'utilisateur est connecté
    if(req.headers.cookie === undefined){
        // Si il n'y a aucun cookie, on utilise le header 'Authorization' dans lequel sera rentré le token généré durant la connexion
        username = jwtUtils.getUserName(req.headers.authorization);
    } else {        
        // Sinon, on parse le cookie pour récupérer le token et vérifier sa validité
        var cookies = req.headers.cookie;
        cookies = cookie.parse(cookies);
        cookies = JSON.parse(cookies.user);
        username = jwtUtils.getUserName(cookies.token);
    }

    // Si l'utilisateur n'est pas connecté ou que le token est invalide, on lui demande de se conneceter
    if(username === undefined){
        res.send({
            success: false,
            error: "Vous devez vous connecter."
        });
    } else {
        var user = {
            username: username
        }
        dataUserLayer.findUser(user, function(dtSet){
            dataTaskLayer.getListSet(dtSet.listes, function(response){
                res.send(response);
            });
        });
    }
});

// Ajoute une liste et créé une première tâche générique
app.post("/addList", function(req,res){
    // On vérifie l'existence du body avant d'essayer de le traiter
    if(req.body && typeof req.body.name != 'undefined'){
        var list = {
            name : req.body.name,
            author : req.body.author,
            tasks : [{
                name: 'Ma première tâche',
                author: req.body.author,
                date: new Date(),
                done: false
            }],
            collaborators: []
        };

        dataTaskLayer.insertList(list, function(response){
            var user = {
                username : req.body.author
            };
            dataUserLayer.addListUser(user,response._id,function(){
                res.send({
                    success: true
                });
            });
        });

    } else {
            res.send({
                success : false,
                error : 'Un ou plusieurs arguments invalides.'
            });
    }
});

// Supprime une liste
app.delete("/deleteList/:list_id", function(req,res){
    var username = undefined;
    if(req.headers.cookie === undefined){
        username = jwtUtils.getUserName(req.headers.authorization);
    } else {
        var cookies = req.headers.cookie;
        cookies = cookie.parse(cookies);
        cookies = JSON.parse(cookies.user);
        username = jwtUtils.getUserName(cookies.token);
    }

    if(username === undefined){
        res.send({
            success: false,
            error: "Vous devez vous connecter."
        });
    } else {
        dataTaskLayer.deleteList(req.params.list_id, function(){
            dataUserLayer.deleteListUser(username, req.params.list_id, function(){
                res.send({
                    success: true
                });
            });
        });
    }
});

// Ajoute la liste au futur collaborateur
app.put("/shareList/:list_id", function(req,res){
    var user = {
        username: req.body.username
    };
    // On vérifie l'existence du futur collaborateur
    dataUserLayer.findUser(user, function(dtSet){
        user = dtSet;
        if(user != null){
            dataUserLayer.addListUser(user, req.params.list_id, function(result){
                // Si le résultat est à égale à 0, update n'a rien modifié donc le collaborateur collaborait déjà
                if(result == 0){
                    res.send({
                        success: false,
                        error: "L'utilisateur collabore déjà sur cette liste !"
                    });
                } else {
                    dataTaskLayer.addCollaborator(req.params.list_id, user, function(result){
                        res.send({
                            success: true
                        });
                    });
                }
            });
        } else {
            res.send({
                success: false,
                error: "Utilisateur inexistant"
            });
        }
    });
});

// Supprime le collaborateur
app.put("/removeCollaborator/:list_id", function(req,res){
    if(req.body && typeof req.body.username != 'undefined'){
        var user = {
            username: req.body.username
        };
        dataUserLayer.deleteListUser(user, req.params.list_id, function(){
            dataTaskLayer.removeCollaborator(req.params.list_id, user, function(){
                res.send({
                    success: true
                });
            });
        });
    } else {
        res.send({
            success : false,
            error : 'Un ou plusieurs arguments invalides.'
        });
    }
}); 

// Ajoute une tâche à une liste
app.post("/addTask", function(req,res){
    if(req.body && typeof req.body._id != 'undefined' && typeof req.body.name != 'undefined'){
        var liste_id = req.body._id;
        var task = {
            name : req.body.name,
            date : new Date(),
            author : req.body.author,
            done : false
        };

        dataTaskLayer.insertTask(liste_id,task, function(){
            res.send({
                success: true
            });
        });

    } else {
            res.send({
                success : false,
                error : 'Un ou plusieurs arguments invalides.'
            });
    }
});

// Supprime une tâche d'un liste
app.delete("/deleteTask/:list_id/:task_id", function(req,res){
    dataTaskLayer.deleteTask(req.params.list_id, req.params.task_id, function(){
        res.send({
            success: true
        });
    });
});

// Modifie une tâche d'une liste
app.put("/updateTask/:list_id", function(req, res){
    if(req.body){
        var task = {
            _id : req.body._id,
            name : req.body.name,
            done: req.body.done
        };

        dataTaskLayer.updateTask(req.params.list_id,task, function(){
            res.send({
                success: true
            });
        });
    }
});

// Créé un utilisateur suite à son inscription
app.post("/createUser", function(req, res){
    if(req.body && typeof req.body.username != 'undefined' && typeof req.body.password != undefined){
        var user = {
            username : req.body.username,
            // Hash le mot de passe
            password : bcrypt.hashSync(req.body.password, saltRounds)
        };  

        var userCheck;
        dataUserLayer.findUser(user,function(dtSet){
            userCheck = dtSet;

            // Un pseudonyme est unique, donc on vérifie si il n'est pas déjà utilisé
            if(userCheck != null && userCheck.username == user.username){
                res.send({
                    success: false,
                    error: "Un utilisateur avec ce pseudonyme existe déjà."
                });
            } else {
                console.log(user);

                dataUserLayer.createUser(user, function(){
                    res.send({
                        success: true,
                        error: "Votre compte a bien été créé."
                    });
                });
            }
        });

    } else {
            res.send({
                success : false,
                error : 'Un ou plusieurs arguments invalides.'
            });
    }
});

// Connexion d'un utilisateur
app.post("/loginUser", function(req, res){
    if(req.body && typeof req.body.username != 'undefined' && typeof req.body.password != undefined){
        user = req.body;
        dataUserLayer.findUser(user,function(dtSet){
            userCheck = dtSet;
            if(userCheck != null   && userCheck.username == user.username){
                // Compare le mot de passe haché en base de données avec le mot de passe entré par l'utilisateur
                var checkPassword = bcrypt.compareSync(user.password, userCheck.password);
                if(checkPassword) {
                    res.send({
                        success: true,
                        _id: userCheck._id,
                        username: userCheck.username,
                        // Le token nous sera utile pour ionic, il sera envoyé dans le header 'Authorization' et aussi dans le cookie si le navigateur le permet
                        token: jwtUtils.generateTokenForUser(userCheck)
                    });
                } else {
                    res.send({
                        success: false,
                        error: "Mauvais mot de passe."
                    });
                }
            } else {
                res.send({
                    success: false,
                    error: "Nom d'utilisateur invalide."
                })
            }
        });
    } else {
        res.send({
            success: false,
            error: "Un ou plusieurs arguments invalides."
        });
    }
});

dataUserLayer.init(function(){
    app.listen(PORT);
    console.log("App listening on port " + PORT);
});

dataTaskLayer.init(function(){
});

