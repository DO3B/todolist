var MongoDB = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://lmayol:mdpsupersecret@cluster0-n3pow.gcp.mongodb.net/test?retryWrites=true";
var client = new MongoClient(uri, {
    useNewUrlParser: true
})
var db;

var dataTaskLayer = {
    
    init : function(callback){
        client.connect(function(err) {
            if(err) throw err;

            db = client.db("todoList");
            callback();
        });
    },
    
    // Permet de récupérer les listes d'un utilisateur
    getListSet : function(list, callback){
        var query = {_id: {$in: list}};
        db.collection("Lists").find(query).toArray(function(err, tasks) {
            if(err) throw err;

            callback(tasks);
        });
    },

    // Créé une liste
    insertList : function(list, callback){
        list.tasks[0]._id = MongoDB.ObjectID();
        db.collection("Lists").insertOne(list, function(err, result) {
            if(err) throw err;

            callback(result.ops[0]);
        });
    },

    // Supprime une liste
    deleteList: function(list_id, callback){
        console.log(list_id);
        var query = { _id: MongoDB.ObjectId(list_id)};
        db.collection("Lists").deleteOne(query, function(err, result){
            if(err) throw err;
            callback();
        });
    },

    getTaskSet : function(task,callback) {
        db.collection("Lists").find(task).toArray(function(err, tasks) {
            if(err) throw err;

            callback(tasks);
        });
    },  
    
    // Insère une tâche dans une liste
    insertTask : function(id,task, callback){
        task._id = MongoDB.ObjectID();
        var query = { _id: MongoDB.ObjectID(id)};
        var change = { $push : {tasks: task}};
        // On utilise updateOne car les listes ont une array de tâches
        db.collection("Lists").updateOne(query,change, function(err, result) {
            if(err) throw err;

            callback();
        });
    },

    // Supprime une tâche
    deleteTask : function(list_id, task_id, callback){
        var query = { _id: MongoDB.ObjectId(list_id)};
        var change = { $pull: {tasks: {_id: MongoDB.ObjectID(task_id)}}};
        db.collection("Lists").updateOne(query, change,function(err, result){
            if(err) throw err;
            callback();
        });
    },

    // Modifie une tâche
    updateTask : function(list_id, task, callback){
        var query = { _id: MongoDB.ObjectId(list_id), "tasks._id": MongoDB.ObjectID(task._id)};
        // $ étant l'index de la tâche trouvée grâce à la query.
        var change = {$set : {"tasks.$.name": task.name, "tasks.$.done": task.done}};
        db.collection("Lists").updateOne(query, change, function(err, result){
                if(err) throw err;
                callback();
            }
        );
    },

    // Ajoute un collaborateur à une liste
    addCollaborator : function(list_id, user, callback){
        var query = { _id: MongoDB.ObjectId(list_id)};
        // addToSet l'ajoute uniquement si il n'est pas déjà dedans pour éviter les doublons
        var change = { $addToSet : {collaborators: user.username}};
        db.collection("Lists").updateOne(query, change, function(err, result){
            if(err) throw err;
            callback();
        });
    },

    // Supprime un collaborateur d'une liste
    removeCollaborator : function(list_id, user, callback){
        var query = { _id: MongoDB.ObjectId(list_id)};
        var change = { $pull : {collaborators: user.username}};
        db.collection("Lists").updateOne(query, change, function(err, result){
            if(err) throw err;
            callback();
        });
    }
};

module.exports = dataTaskLayer;