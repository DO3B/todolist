var MongoDB = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var uri = "mongodb+srv://lmayol:mdpsupersecret@cluster0-n3pow.gcp.mongodb.net/test?retryWrites=true";
var client = new MongoClient(uri, {
    useNewUrlParser: true
})
var db;

var dataUserLayer = {

    init : function(callback){
        client.connect(function(err) {
            if(err) throw err;

            db = client.db("todoList");
            callback();
        });
    },

    // Créé un utilisateur
    createUser : function(user, callback){
        user.listes = [];
        db.collection("User").insertOne(user, function(err,result){
            if(err) throw err;

            callback();
        });
    },

    // Ajoute une liste à un utilisateur
    addListUser : function(user,list_id,callback){
        var query = { username: user.username};
        var change = {$addToSet : {listes: MongoDB.ObjectID(list_id)}};
        db.collection("User").updateOne(query, change, function(err,result){
            callback(result.modifiedCount);
        });
    },

    // Supprime une liste d'un utilisateur
    deleteListUser: function(user, list_id, callback){
        var query = { username: user.username};
        var change = {$pull: {listes: {$in: [MongoDB.ObjectID(list_id)]}}};
        db.collection("User").updateOne(query, change, function(err,result){
            callback();
        });
    },

    // Permet de trouver un utilisateur
    findUser : function(user,callback){
        var query = { username: user.username};
        db.collection("User").findOne(query, function(err, result){   
            if(err) throw err;
            
            callback(result);

        });
    }
};

module.exports = dataUserLayer;