var serverNode = "http://todolist-mayol.herokuapp.com";

angular.module('starter.controllers', [])

.controller('ConnectionCtrl',['$window','$scope', '$state', 'connectionFactory', function($window,$scope,$state,connectionFactory){
    $scope.user = {};
    $scope.userR = {};
    $scope.errorData = {};
    
    $scope.login = function(){
        var user = $scope.user;
        
        if(user.username && user.password){
            connectionFactory.login(user.username, user.password, function(response){
                if(response.data.success){
                    $window.localStorage.setItem('token',response.data.token);
                    $window.localStorage.setItem('username', response.data.username);
                    $state.go('todoList');
                } else {
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };
                    alert($scope.errorData.error);
                }
            });
        } else {
            $scope.errorData = {
                success: false,
                error: "Merci de compléter les deux champs."
            };
            alert($scope.errorData.error);
        }
    };

    $scope.createUser = function(){
        var user = $scope.userR;
        if(user.username && user.password){
            connectionFactory.create(user.username, user.password, function(response){
                if(response.data.success){
                    $scope.user = $scope.userR;
                    $('#Modal').modal('hide');
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };               
                    alert($scope.errorData.error);
                } else {
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };                    
                    alert($scope.errorData.error);
                }
            });
        }
    };
}])

.controller('TodoCtrl', ['$ionicModal','$window', '$scope', '$state', 'todoFactory', function ($ionicModal,$window, $scope, $state, todoFactory) {
    $scope.listData = {};
    $scope.userData = {};
    $scope.user = {
        username: $window.localStorage.getItem('username'),
        token: $window.localStorage.getItem('token')
    };

    $scope.refreshTaskSet = function () {
        if ($scope.user.token != "") {
            todoFactory.getTaskSet($scope.user.token, function (response) {
                $scope.laliste = response.data;
            });
            $scope.listData = {};
            $scope.userData = {};
        } else {
            alert("Merci de vous reconnecter.")
        }
    },

    $scope.createList = function (){
        if ($scope.user.token != "") {
            var list = {
                name: $scope.listData.name,
                author: $scope.user.username
            };
            todoFactory.createList(list, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.");
        }
    },

    $scope.deleteList = function (list) {
        if ($scope.user.token != "") {
            if(list.author == $scope.user.username){
                if(list.collaborators.length == 0){
                    todoFactory.deleteList($scope.user.token, list._id, function (response) {
                        $scope.refreshTaskSet();
                    });
                } else {
                    alert("Vous ne pouvez pas supprimer une liste qui possède des collaborateurs. Merci de les retirer avant de réessayer.")
                }
            } else {
                todoFactory.removeCollaborator(list, {username: $scope.user.username},function(response){
                    $scope.refreshTaskSet();
                });
            }
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $ionicModal.fromTemplateUrl('templates/modalList.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.listModal = modal;
      });
    
    $scope.openCollabList = function(liste){
        $scope.liste = liste;
        $scope.listModal.show();
    }

    $scope.shareList = function(list_id){
        if ($scope.user.token != "") {
            var user = {
                username: $scope.userData.username
            };
            todoFactory.shareList(list_id, user,function(response){
                $scope.userData = {};
                $scope.refreshTaskSet();
                $scope.listModal.hide();
            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $scope.removeCollaborator = function(list_id, collaborator){
        if ($scope.user.token != "") {
            var user = {
                username: collaborator
            };
            todoFactory.removeCollaborator(list_id, user,function(response){
                $scope.refreshTaskSet();
                $scope.listModal.hide();
            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $ionicModal.fromTemplateUrl('templates/modalTasks.html', {
        scope: $scope
      }).then(function(modal) {
        $scope.taskModal = modal;
      });
        
      $scope.openTaskList = function(liste){
        $scope.liste = liste;
        $scope.taskModal.show();
    }

    $scope.createTask = function (list_id, newtask) {
        if ($scope.user.token != "") {
            var task = {
                _id: list_id,
                name: newtask,
                author: $scope.user.username
            };
            todoFactory.createTask(task, function (response) {
                $scope.refreshTaskSet();
                $scope.taskModal.hide();

            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $scope.deleteTask = function (list_id, task_id) {
        if ($scope.user.token != "") {
            todoFactory.deleteTask(list_id,task_id, function (response) {
                $scope.refreshTaskSet();
                $scope.taskModal.hide();

            });
        } else {
            alert("Merci de vous reconnecter.")

        }
    },

    $scope.updateTask = function (list_id,taskData) {
        if ($scope.user.token != "") {

            todoFactory.updateTask(list_id,taskData, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.")

        }
    },

    $scope.logout = function () {
        if ($scope.user.token != "") {
            $window.localStorage.removeItem('token');
            $window.localStorage.removeItem('username');
            
        }
        $state.go('connection');
    },

    $scope.refreshTaskSet();

}]);
