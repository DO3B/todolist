angular.module('starter.services', [])

.factory('connectionFactory', ['$http', function($http) {
    var factory = {};

    factory.create = function(username, password, callback) {
        var query = {
            username: username,
            password: password
        }
        $http({
            method: 'POST',
            url: serverNode + '/createUser',
            data: query
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    };

    factory.login = function(username, password, callback){
        var query = {
            username: username,
            password: password
        }

        $http({
            method: 'POST',
            url: serverNode + '/loginUser',
            data: query
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    };

    return factory;
}])

.factory('todoFactory', ['$http', function($http) {
    var factory = {};

    factory.getTaskSet = function(token,callback){
        $http({
            method: 'GET',
            url: serverNode + '/getTaskSet',
            headers: {
                'Authorization': token
            }
        }).then(function successCallback(response){
            callback(response)
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.createList = function(list, callback){
        $http({
            method: 'POST',
            url: serverNode + '/addList',
            data: list
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.deleteList = function(token, list_id, callback){
        $http({
            method: 'DELETE',
            url: serverNode +  '/deleteList/' + list_id,
            headers: {
                'Authorization': token
            }
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.shareList = function(list_id, user,callback){
        $http({
            method: 'PUT',
            url: serverNode + '/shareList/' + list_id,
            data: user
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.removeCollaborator = function(list_id, user, callback){
        $http({
            method: 'PUT',
            url: serverNode + '/removeCollaborator/' + list_id,
            data: user
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.createTask = function(list,callback){
        $http({
            method: 'POST',
            url: serverNode + '/addTask',
            data: list,
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.deleteTask = function(list_id, task_id, callback){
        $http({
            method: 'DELETE',
            url: serverNode + '/deleteTask/' + list_id +"/"+ task_id,
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.updateTask = function(list_id,task, callback){
        $http({
            method: 'PUT',
            url: serverNode + '/updateTask/'+ list_id,
            data: task
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    }

    return factory;
}]);