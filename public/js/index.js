var todoApp = angular.module('todoApp', ['ui.router','ngCookies']);

todoApp.config(function($stateProvider){

    var connectionState = {
        name: "connection",
        url: "/login",
        templateUrl: "connection.html",
        controller: "ConnectionCtrl"
    };

    var todoState = {
        name: "todoList",
        url: "/todoList",
        templateUrl: "todo.html",
        controller: "TodoCtrl"
    }

    $stateProvider.state(connectionState);
    $stateProvider.state(todoState);
    
});

todoApp.run(['$cookies','$state', '$timeout' , function($cookies,$state, $timeout) {
    // On verifie si le cookie existe et si il contient un token
    var user = $cookies.getObject('user');
    if(user) {
        $state.go('todoList');
    }
    else {
        $timeout(function(){
            alert("Veuillez vous connecter. Vous allez être redirigé sur la page de connexion.")
            $state.go('connection');
        },3000);
    }
}]);
