// La connectionFactory nous servira a formulé les requêtes HTTP pour le connectionCtrl
todoApp.factory('connectionFactory', ['$http', function($http) {
    var factory = {};

    factory.create = function(username, password, callback) {
        var query = {
            username: username,
            password: password
        }
        $http({
            method: 'POST',
            url: '/createUser',
            data: query
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    };

    factory.login = function(username, password, callback){
        var query = {
            username: username,
            password: password
        }

        $http({
            method: 'POST',
            url: '/loginUser',
            data: query
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    };

    return factory;
}]);