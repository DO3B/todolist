// La todoFactory nous servira a formulé les requêtes HTTP pour le todoCtrl
todoApp.factory('todoFactory', ['$http', function($http) {
    var factory = {};

    factory.getTaskSet = function(token,callback){
        $http({
            method: 'GET',
            url: '/getTaskSet',
        }).then(function successCallback(response){
            callback(response)
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.createList = function(list, callback){
        $http({
            method: 'POST',
            url: '/addList',
            data: list
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.deleteList = function(list_id, callback){
        $http({
            method: 'DELETE',
            url: '/deleteList/' + list_id,
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.shareList = function(list_id, user,callback){
        $http({
            method: 'PUT',
            url: '/shareList/' + list_id,
            data: user
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.removeCollaborator = function(list_id, user, callback){
        $http({
            method: 'PUT',
            url: '/removeCollaborator/' + list_id,
            data: user
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.createTask = function(list,callback){
        $http({
            method: 'POST',
            url: '/addTask',
            data: list
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.deleteTask = function(list_id, task_id, callback){
        $http({
            method: 'DELETE',
            url: '/deleteTask/' + list_id +"/"+ task_id
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    },

    factory.updateTask = function(list_id,task, callback){
        $http({
            method: 'PUT',
            url: '/updateTask/'+ list_id,
            data: task
        }).then(function successCallback(response){
            callback(response);
        }, function errorCallback(err){
            console.log('Error: ' + err.data.error);
        });
    }

    return factory;
}]);