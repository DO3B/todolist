todoApp.controller('TodoCtrl', ['$cookies', '$scope', '$state', 'todoFactory', function ($cookies, $scope, $state, todoFactory) {
    $scope.listData = {};
    $scope.userData = {};
    $scope.user = $cookies.getObject('user');

    $scope.refreshTaskSet = function () {
        if ($scope.user.token != "") {
            todoFactory.getTaskSet($scope.user.token, function (response) {
                $scope.laliste = response.data;
            });
            $scope.listData = {};
            $scope.userData = {};
        } else {
            alert("Merci de vous reconnecter.")
        }
    },

    $scope.createList = function (){
        if ($scope.user.token != "") {
            var list = {
                name: $scope.listData.name,
                author: $scope.user.username
            };
            todoFactory.createList(list, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.");
        }
    },

    $scope.deleteList = function (list) {
        if ($scope.user.token != "") {
            if(list.author == $scope.user.username){
                if(list.collaborators.length == 0){
                    todoFactory.deleteList(list._id, function (response) {
                        $scope.refreshTaskSet();
                    });
                } else {
                    alert("Vous ne pouvez pas supprimer une liste qui possède des collaborateurs. Merci de les retirer avant de réessayer.")
                }
            } else {
                todoFactory.removeCollaborator(list, {username: $scope.user.username},function(response){
                    $scope.refreshTaskSet();
                });
            }
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $scope.shareList = function(list_id){
        if ($scope.user.token != "") {
            var user = {
                username: $scope.userData.username
            };
            todoFactory.shareList(list_id, user,function(response){
                $scope.userData = {};
                $scope.refreshTaskSet();
                $('#Modal-collab' + list_id).modal('toggle');
            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $scope.removeCollaborator = function(list_id, collaborator){
        if ($scope.user.token != "") {
            var user = {
                username: collaborator
            };
            todoFactory.removeCollaborator(list_id, user,function(response){
                $scope.refreshTaskSet();
                $('#Modal-collab' + list_id).modal('toggle');
            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },
    $scope.createTask = function (list_id, newtask) {
        if ($scope.user.token != "") {
            var task = {
                _id: list_id,
                name: newtask,
                author: $scope.user.username
            };
            todoFactory.createTask(task, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.");

        }
    },

    $scope.deleteTask = function (list_id, task_id) {
        if ($scope.user.token != "") {
            todoFactory.deleteTask(list_id,task_id, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.")

        }
    },

    $scope.updateTask = function (list_id,taskData) {
        if ($scope.user.token != "") {

            todoFactory.updateTask(list_id,taskData, function (response) {
                $scope.refreshTaskSet();
            });
        } else {
            alert("Merci de vous reconnecter.")

        }
    },

    $scope.logout = function () {
        if ($scope.user.token != "") {
            $cookies.remove('user');
        }
        $state.go('connection');
    },

    $scope.refreshTaskSet();

}]);
