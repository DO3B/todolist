todoApp.controller('ConnectionCtrl',['$cookies','$scope', '$state', 'connectionFactory', function($cookies,$scope,$state,connectionFactory){
    $scope.user = {};
    $scope.userR = {};
    $scope.errorData = {};
    
    $scope.login = function(){
        var user = $scope.user;

        if(user.username && user.password){
            connectionFactory.login(user.username, user.password, function(response){
                if(response.data.success){
                    var cookie = {
                        token: response.data.token,
                        username: response.data.username
                    };
                    var now = new Date();
                    //On fait un cookie qui dure qu'une journée.
                    var exp = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);
                    $cookies.putObject('user', cookie, {'expires': exp});
                    $state.go('todoList');
                } else {
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };
                    $('#collapseErrorLogin').collapse();              
                }
            });
        } else {
            $scope.errorData = {
                success: false,
                error: "Merci de compléter les deux champs."
            };
            $('#collapseErrorLogin').collapse();
        }
    };

    $scope.createUser = function(){
        var user = $scope.userR;
        if(user.username && user.password){
            connectionFactory.create(user.username, user.password, function(response){
                if(response.data.success){
                    $scope.user = $scope.userR;
                    $('#Modal').modal('hide');
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };               
                    $('#collapseErrorLogin').collapse();  
                } else {
                    $scope.errorData = {
                        success: response.data.success,
                        error: response.data.error
                    };                    
                    $('#collapseErrorInscription').collapse();
                }
            });
        }
    };
}]);