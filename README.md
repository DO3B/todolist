# Todo List

## Dépendances 

L'application nécéssite l'installation des modules suivants :

- Express,
- MongoDB,
- Bcrypt,
- jsonwebToken,
- jwt-utils.

Pour les installer, il suffit de se placer dans le répertoire du projet et d'éxécuter la commande ci-dessous :

`
npm install
`

Puis lancer le serveur :

`
npm start
`

## Heroku

L'application est disponible au lien suivant :
[http://todolist-mayol.herokuapp.com/](http://todolist-mayol.herokuapp.com/)

Pour la version Ionic, il suffit d'aller sur le lien suivant (swipe sur une liste pour la partager) :
[http://todolistionic-mayol.herokuapp.com](http://todolistionic-mayol.herokuapp.com)

## Ionic

**Attention ! Pour utiliser la version Ionic de l'application, une connexion Internet est nécessaire (non-universitaire pour la base de données).**


Pour essayer la version **Ionic** de l'application, il faut se placer dans le répertoire `/ionic` puis lancer la commande ci-dessous :

`
npm install
`

Puis lancer **Ionic** :

`
ionic serve
`
